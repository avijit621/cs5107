// This is the driver programme which creates all the processes
// We haveto execute the follwoing command to run it : gcc driver.c -lpthread -o driver
/* The program is creating four threads : GOD ,Hare,Totoise and Reporter
     we are using Three mutexes in this programme 
     */
#include<stdio.h>
#include<stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>



long hare = 0;//varibale to measure hare's distance 
long tortoise = 0; //variable to measure Tortoise's position

long hare_time = 0;//variable for time taken for hare
long tortoise_time = 0; //variable for time taken for Tortoise

// The following are the constants that god can use to make the hare sleep or
// change its position
const long TARGET = 4e8;
const long STEPS = 5;
const long SlEEP_DISTANCE = 2e8;

// mutexes to print value in terminal
pthread_mutex_t mut_tortoise = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t  mut_hare = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t print_mut = PTHREAD_MUTEX_INITIALIZER;


long gen_ran_pos()  //A funcion to generate random position
     {             
    return rand() % (TARGET+1);
      }


void *tortoise_turn(void *args) // Thread creation for Tortoise
   {
     while(tortoise < TARGET) 
     {
        pthread_mutex_lock (&mut_tortoise);
        tortoise++;
        tortoise_time++;
        pthread_mutex_unlock (&mut_tortoise);
    }
    return (void *) 0;
}

void *hare_turn(void *args)  // Thread creation for Hare
   {
     while(hare < TARGET) 
     {
        if(hare-tortoise >= SlEEP_DISTANCE) 
        {
            long sleep_time = rand()%(100000);//if Hare is ahead He can sleep for 
            hare_time += sleep_time;          // any random amount of

            long random_sleep = rand()%1000; // Generating time for his sleep
            usleep(random_sleep);
        }
        pthread_mutex_lock (&mut_hare);
        hare += STEPS;
        hare_time++;
        pthread_mutex_unlock (&mut_hare);
    }
    return (void *) 0;

}


void *reporter_turn(void *args) // Thread creation for reporter
{
   while(tortoise < TARGET || hare < TARGET) 
   {
        pthread_mutex_lock (&print_mut);

        printf ("\n -------Race Status------\n");
        printf("\n Tortoise has reached : %ld \t, in time : %lu and steps taken",tortoise,tortoise_time);
        printf("\n Tortoise is at position : %ld \t, at time : %lu and steps taken",hare,hare_time);

        pthread_mutex_unlock (&print_mut);
        usleep(500);
    }
    return (void *) 0;

}


void *god_turn(void *args)  //Thread creation for GOD
{

    while(tortoise < TARGET || hare < TARGET) 
     {   // God locks the thread before making changes
        pthread_mutex_lock (&print_mut);
        pthread_mutex_lock (&mut_tortoise);
        pthread_mutex_lock (&mut_hare);

        if(( (double) rand() / (RAND_MAX) ) >= 0.75) 
        {
            printf("\n God has changed positions...\n");
            printf("\n-------NEW POSITIONS --------\n\n");
            if(tortoise < TARGET) 
            {
                tortoise = gen_ran_pos();
                printf(" Tortoise =  %ld\n",tortoise);
            }
            if(hare < TARGET) 
            {
                hare     = gen_ran_pos();
                (" Hare     = %ld \n",hare);
            }

        } 
        else
         {

            // unlock all mutexes locked by god
            pthread_mutex_unlock (&print_mut);
            pthread_mutex_unlock (&mut_tortoise);
            pthread_mutex_unlock (&mut_hare);

            // when god doesn't want to change the positions of hare and tortoise,
            // then we will allow god to sleep for a while
            usleep(500);
            continue;

        }

        pthread_mutex_unlock (&print_mut);
        pthread_mutex_unlock (&mut_tortoise);
        pthread_mutex_unlock (&mut_hare);
    
    }
    return (void *) 0;

}

// main function
int main() {

    /* initialize random seed */
    srand (time(NULL));

    // thread ids for tortoise, hare, reporter, god threads
    pthread_t tortoise_tid, hare_tid, reporter_tid, god_tid;

    // creating four threads
    pthread_create (&tortoise_tid, NULL, tortoise_turn, NULL);
    pthread_create (&hare_tid, NULL, hare_turn, NULL);
    pthread_create (&reporter_tid, NULL, reporter_turn, NULL);
    pthread_create (&god_tid, NULL, god_turn, NULL);

    // wait for each thread to finish their execution
    pthread_join (tortoise_tid, NULL);
    pthread_join (hare_tid, NULL);
    pthread_join (reporter_tid, NULL);
    pthread_join (god_tid, NULL);

    printf("\n--------- RACE HAS FINISHED ---------------\n");
    printf("\n The results of the race are as follows : \n\n");
    printf("\tTime taken by tortoise :   %ld  iterations\n",tortoise_time);
    printf("\tTime taken by hare    %ld :  iterations\n",hare_time);

    if (tortoise_time < hare_time) {
        printf ( "\n\tWINNER of the race is 'tortoise'\n");
    } else if (tortoise_time > hare_time) {
        printf ( "\n\tWINNER of the race is 'hare'.\n");
    } else {
        printf("\n\tThe race has finished as a Tie\n");
    }
    printf( "\n---------------------------------------------------\n");
    return 0;

}
