//=================== DRIVER PROGRAM ===================
// This program is for LINUX Operating System.
// Run program using these two commands : (1) make (2) ./driver
/* This is the main program which creates four processes :
      1. Tortoise
      2. Hare
      3. Reporter
      4. God    

We use named pipes (also known as 'fifo') for interprocess communication between different processes. 
Eight pipes are being used for that purpose.
*/
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <signal.h>

//necessary pipes for our processes
const char *race_god = "/tmp/race_god";
const char *tortoise_rw  = "/tmp/tortoise_rw";
const char *tortoise_wr  = "/tmp/tortoise_wr";
const char *hare_rw = "/tmp/driver_hare_rw";
const char *hare_wr = "/tmp/driver_hare_wr";
const char *hare_tortoise = "/tmp/hare_tortoise";
const char *driver_reporter_wr = "/tmp/driver_reporter_wr";
const char *driver_reporter_rw = "/tmp/driver_reporter_rw";

char *tortoise_args[] = { "./tortoise.out", NULL };
char *hare_args[]     = { "./hare.out", NULL };
char *reporter_args[] = { "./reporter.out", NULL };
char *god_args[]      = { "./god.out", NULL };

int main() 
{

    mkfifo(race_god, 0666);
    mkfifo(tortoise_rw, 0666);
    mkfifo(tortoise_wr, 0666);
    mkfifo(hare_rw, 0666);
    mkfifo(hare_wr, 0666);
    mkfifo(hare_tortoise, 0666);
    mkfifo(driver_reporter_rw, 0666);
    mkfifo(driver_reporter_wr, 0666);

    int tortoise = 0;
    int hare = 0;
    const int TARGET = 30;
    int pid_tortoise, pid_hare, pid_reporter, pid_god;

    //  call tortoise
    if((pid_tortoise = fork()) == 0)
     {
        execv (tortoise_args[0], tortoise_args);
    }
    int write_tortoise = open(tortoise_wr, O_WRONLY);
    int read_tortoise  = open(tortoise_rw, O_RDONLY);
    write(write_tortoise, &tortoise, sizeof(int));

    // call Hare 
    if((pid_hare = fork()) == 0) 
    {
        execv (hare_args[0], hare_args);
    }
    int write_hare = open(hare_wr, O_WRONLY);
    int read_hare  = open(hare_rw, O_RDONLY);
    write(write_hare, &hare, sizeof(int));

    // call Reporter
    if((pid_reporter = fork()) == 0) 
    {
        execv (reporter_args[0], reporter_args);
    }
    int write_reporter = open(driver_reporter_wr, O_WRONLY);
    int read_reporter  = open(driver_reporter_rw, O_RDONLY);

    // winner = 0 for Tortoise
    // winner = 1 for Hare
    int winner = 0;
    int share_msg = 0;
    while(1) 
    {

        // wait for tortoise and hare to make their move
        read(read_tortoise, &tortoise, sizeof(int));
        read(read_hare, &hare, sizeof(int));

        char flag ;
        printf("Position change argument for GOD ? (y/n)  ");
        scanf("%c",&flag);
        if(flag == 'y') {
            if((pid_god = fork()) == 0) 
            {
                execv(god_args[0], god_args);
            }
            int read_god = open(race_god, O_RDONLY);
            read(read_god, &tortoise, sizeof(int));
            read(read_god, &hare, sizeof(int));
            close(read_god);
            wait(NULL);
        }
        
        if(hare >= TARGET) {
            winner = 1;
            break;
        } else if(tortoise >= TARGET) {
            break;
        }
        // Writing the values corresponding to hare and tortoise in the pipe for Reporter
        write(write_reporter, &tortoise, sizeof(int));
        write(write_reporter, &hare, sizeof(int));

        // Writing the values corresponding to hare and tortoise in their corresponfing pipes
        write(write_tortoise, &tortoise, sizeof(int));
        write(write_hare, &hare, sizeof(int));

        // Reporter displays the status
        read(read_reporter, &share_msg, sizeof(int));
    }

    printf("\nRace is won by : %s", (winner == 0) ? "Tortoise\n" : "Hare\n");

    // kill the tree processes
    kill(pid_tortoise, SIGTERM);
    kill(pid_hare, SIGTERM);
    kill(pid_reporter, SIGTERM);

    // close all the pipe descriptors
    close(write_reporter);
    close(read_hare);
    close(read_tortoise);
    close(write_hare);
    close(write_tortoise);
    return 0;
}
