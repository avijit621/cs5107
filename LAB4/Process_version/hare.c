#include <stdio.h>
#include <unistd.h>
#include<stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>


const char *race_god          = "/tmp/race_god";
const char *tortoise_rw  = "/tmp/tortoise_rw";
const char *tortoise_wr  = "/tmp/tortoise_wr";
const char *hare_rw    = "/tmp/driver_hare_rw";
const char *hare_wr     = "/tmp/driver_hare_wr";
const char *hare_tortoise       = "/tmp/hare_tortoise";
const char *driver_reporter_wr  = "/tmp/driver_reporter_wr";
const char *driver_reporter_rw  = "/tmp/driver_reporter_rw";

int main() {

    int read_driver    = open(hare_wr   , O_RDONLY);
    int write_driver   = open(hare_rw  , O_WRONLY);
    int read_tortoise  = open(hare_tortoise, O_RDONLY);

    int hare = 0;
    int tortoise = 0;
    const int steps = 3;
    const int SLEEP_DIS = 10;
    const int SLEEP_TIME = 5;
    int sleep_counter = -1;

    // reader for hare_tortoise pipe
    while(true) 
    {
        // The hare sleeps for some time and for that purpose we use the sleep counter 
       
        if(sleep_counter != -1) 
        {
            sleep_counter++;
            read(read_driver, &hare, sizeof(int));
            read(read_tortoise, &tortoise, sizeof(int));
            write(write_driver, &hare, sizeof(int));
            if(sleep_counter >= SLEEP_TIME) 
            {
                sleep_counter = -1;
            }
            continue;
        }
        read(read_driver, &hare, sizeof(int));
        read(read_tortoise, &tortoise, sizeof(int));
        hare += steps;
        write(write_driver, &hare, sizeof(int));
        if(hare-tortoise >= SLEEP_DIS) 
        {
            sleep_counter = 0;
        }
    }

    close(read_driver);
    close(write_driver);
    close(read_tortoise);
    return 0;
}
