#include <stdio.h>
#include<stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>


const char *race_god  = "/tmp/race_god";
const char *tortoise_rw = "/tmp/tortoise_rw";
const char *tortoise_wr = "/tmp/tortoise_wr";
const char *hare_rw = "/tmp/driver_hare_rw";
const char *hare_wr = "/tmp/driver_hare_wr";
const char *hare_tortoise = "/tmp/hare_tortoise";
const char *driver_reporter_wr = "/tmp/driver_reporter_wr";
const char *driver_reporter_rw = "/tmp/driver_reporter_rw";

int main() 
{


    int read_driver = open(driver_reporter_wr, O_RDONLY);
    int write_driver = open(driver_reporter_rw, O_WRONLY);

    int share_msg = 0;
    int tortoise, hare;
    while(true) 
    {
       read(read_driver, &tortoise, sizeof(int));
       read(read_driver, &hare, sizeof(int)); 
       printf("\n\t The current status is \t\n");
       printf("Tortoise = %d \n",tortoise);
       printf("Hare = %d \n",hare);

       write(write_driver, &share_msg, sizeof(int));
       if(tortoise >= 30 || hare >= 30) {
           break;
       }
    }
    close(read_driver);
    return 0;
}
